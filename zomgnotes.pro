TARGET = zomgnotes
TEMPLATE = app

# WARNING: the createpackage script used to generate symbian packages seems
# to only accept the format NUMBER.NUMBER.NUMBER for the version field
VERSION = 0.4.0

# Pass version to zomgnotes during compilation
DEFINES += ZOMGNOTES_VERSION=\'\"$$VERSION\"\'

# Sets minimum heap size to 2MB, and maximum to 32MB (on symbian)
TARGET.EPOCHEAPSIZE = 0x200000 0x2000000

# Compile flags for GCC on linux
QMAKE_CXXFLAGS.GCC += -Wall
# Compile flags for target hardware
QMAKE_CXXFLAGS.ARMCC += -Wall -O2

CONFIG += debug

SOURCES += src/mainwindow.cpp \
    src/drawwidget.cpp \
    src/notelistwidget.cpp \
    src/shownotewidget.cpp \
    src/note.cpp \
    src/notekeeper.cpp
    
HEADERS += src/mainwindow.h \
    src/drawwidget.h \
    src/notelistwidget.h \
    src/shownotewidget.h \
    src/note.h \
    src/notekeeper.h \
    src/global.h

RESOURCES += res.qrc
