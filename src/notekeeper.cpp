/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "notekeeper.h"

#include <stdlib.h>

#include <QtDebug>
#include <QDir>
#include <QFile>
#include <QDate>
#include <QMap>
#include <QMessageBox>
#include <QImageReader>
#include <QSettings>

#include "global.h"

QList<Note*> NoteKeeper::_noteList = QList<Note*>();
QString NoteKeeper::_dataDir = QString();

void NoteKeeper::initialize() {
	//qDebug() << "NoteKeeper::initialize";
	QSettings settings;

	if (settings.contains("notekeeper/datadir")) {
		QVariant value = settings.value("notekeeper/datadir");
		if (value.canConvert(QVariant::String)) {
			_dataDir = value.toString();
		} else {
			QMessageBox::critical(0, QObject::tr("Critical error"), QObject::tr("Invalid entry in settings."));
			return;
		}
	} else {
#ifdef Q_OS_SYMBIAN
		_dataDir = "e:/";
#else
		_dataDir = QString();
#endif
	}

	if (QDir(_dataDir).isReadable()) {
		settings.setValue("notekeeper/datadir", _dataDir);
	} else {
		QMessageBox::critical(0, QObject::tr("Error"), QObject::tr("Cannot read note directory, please check your settings. Notes will not be saved successfully"));
		return;
	}

	if (_dataDir.isEmpty()) _dataDir = ".";
	_dataDir += "/";
	
	loadNotes();
}

void NoteKeeper::loadNotes() {
	QDir dir(_dataDir);
	QStringList filters;
	filters << "*.png.note";

	foreach (QString file, dir.entryList(filters, QDir::Files)) {
		//qDebug() << file;

		// Code assumes that _dataDir is never empty (when using current directory, it should be '.')
		QImage noteFile(_dataDir + file);
		if (noteFile.isNull()) {
			qDebug() << "Null note:" << file;
			continue;
		}
		
		Note *note = new Note();
		note->_id = noteFile.text("zomgnotes::id");
		if (note->id().isEmpty()) {
			qDebug() << "Invalid or corrupt note:" << file;
			continue;
		}

		note->_date = QDateTime::fromString(noteFile.text("zomgnotes::date"), Qt::ISODate);
		note->_title = noteFile.text("zomgnotes::title");
		note->_category = noteFile.text("zomgnotes::category");
		note->_image = QImage(noteFile).convertToFormat(NOTE_IMAGE_FORMAT);

		// Keep original image metadata, in an attempt to make interchange of files with newer
		// program versions possible (that might have added more metadata to the stream)
		QMap<QString,QString> metaInfo;
		foreach (QString key, noteFile.textKeys()) {
			metaInfo[key] = noteFile.text(key);
		}
		note->_additionalMetadata = metaInfo;

		_noteList.append(note);
	}
}

Note* NoteKeeper::newNote() {
	Note *note = new Note();

	note->_date = QDateTime::currentDateTime();
	note->_id = note->_date.date().toString("ddMM") + QString::number(randomNumber());
	note->_title = note->_date.time().toString("H:mm") + " ";
	note->_category = "Unfiled";
	note->_image = QImage(640, 1024, NOTE_IMAGE_FORMAT);
	note->_image.fill(1);

	saveNote(note);
	_noteList.append(note);

	return note;
}

QStringList NoteKeeper::categories() {
	QStringList cat;
	cat << "All" << "Unfiled" << "Work" << "Personal";
	return cat;
}

QList<Note*> NoteKeeper::notesForCategory(QString category) {
	QList<Note*> lst;
	bool all = (category == "All");
	foreach (Note *note, _noteList) {
		if (all || (note->category() == category)) lst.append(note);
	}
	qStableSort(lst.begin(), lst.end(), Note::dateLessThan);
	return lst;
}

Note* NoteKeeper::getNote(QString noteId) {
	foreach (Note *note, _noteList) {
		if (note->id() == noteId) return note;
	}
	return 0;
}

bool NoteKeeper::saveNote(Note *note) {
	qDebug() << "saveNote" << note->id();
	QFile outputFile(_dataDir + note->id() + ".png.note");

	if (!outputFile.open(QIODevice::WriteOnly)) {
		QMessageBox::critical(0, QObject::tr("Critical error"), QObject::tr("Unable to save note."));
		return false;
	}

	// Merge new/updated metadata with the already existing metadata, so as to not drop metadata
	// that may have come from newer program versions
	QMap<QString,QString> metaInfo = note->_additionalMetadata;

	metaInfo["zomgnotes::id"] = note->id();
	metaInfo["zomgnotes::title"] = note->title();
	metaInfo["zomgnotes::category"] = note->category();
	metaInfo["zomgnotes::date"] =  note->date().toString(Qt::ISODate);

	QImage outputImage(note->image());
	QMapIterator<QString, QString> i(metaInfo);
	while (i.hasNext()) {
		i.next();
		outputImage.setText(i.key(), i.value());
	}

	if (!outputImage.save(&outputFile, "PNG")) {
		QMessageBox::critical(0, QObject::tr("Critical error"), QObject::tr("Unable to save note."));
		return false;
	}

	outputFile.close();

	return true;
}

bool NoteKeeper::deleteNote(Note *note) {
	bool result = QFile::remove(_dataDir + note->id() + ".png.note");
	if (!result) {
		QMessageBox::critical(0, QObject::tr("Critical error"), QObject::tr("Unable to delete note."));
	} else {
		_noteList.removeOne(note);
		delete note;
	}
	return result;
}

int NoteKeeper::randomNumber() {
	static bool randomInitialized = false;
	if (!randomInitialized) {
		srandom(QDateTime::currentDateTime().toTime_t());
		randomInitialized = true;
	}
	return random();
}

// kate: space-indent off; indent-width 8; replace-tabs off;
