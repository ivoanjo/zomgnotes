/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawwidget.h"

#include <QMouseEvent>
#include <QPainter>
#include <QPaintEvent>
#include <QMessageBox>
#include <QApplication>

#include "note.h"

DrawWidget::DrawWidget(QWidget *parent) : QLabel(parent), _mouseDown(false) {
	setAttribute(Qt::WA_StaticContents);
}

void DrawWidget::setImage(QImage newImage) {
	// Convert image to an indexed format, to easily do the changing from black to blue
	// There is probably a better way to do this
	QImage indexed = newImage.convertToFormat(QImage::Format_Indexed8);
	indexed.setNumColors(2);
	if (indexed.color(0) == qRgb(255,255,255)) indexed.setColor(1, qRgb(51,0,153));
	else indexed.setColor(0, qRgb(51,0,153));

	// Now create the pixmap
	_pixmap = QPixmap::fromImage(indexed);

	if (_pixmap.isNull()) {
		QMessageBox::critical(0, tr("Critical error"), tr("Unable to show note."));
		QApplication::exit(1);
	}

	setMinimumHeight(_pixmap.height());
	update();
}

QImage DrawWidget::getImage() {
	return _pixmap.toImage().convertToFormat(NOTE_IMAGE_FORMAT, Qt::MonoOnly | Qt::ThresholdDither | Qt::AvoidDither);
}

void DrawWidget::draw(const QPoint & p1, const QPoint & p2) {
	static QPen pen(QColor(qRgb(51,0,153)), 3);

	// Calculate top-left point and bottom-right one, which p1 and p2 may not represent
	QPoint topLeft(qMin(p1.x(), p2.x()), qMin(p1.y(), p2.y()));
	QPoint btmRight(qMax(p1.x(), p2.x()), qMax(p1.y(), p2.y()));
	
	QPainter painter(&_pixmap);
	painter.setPen(pen);
	painter.drawLine(p1, p2);
	update(QRect(topLeft, btmRight).adjusted(-pen.width(), -pen.width(), pen.width(), pen.width()));
}

void DrawWidget::mouseMoveEvent(QMouseEvent *event) {
	//qDebug() << event->button() << event->x() << event->y();
	
	if (_mouseDown) {
		draw(_lastPoint, event->pos());
		_lastPoint = event->pos();
	}
}

void DrawWidget::mousePressEvent(QMouseEvent *event) {
	//qDebug() << "mousePressEvent" << event->button() << event->x() << event->y();
	
	_mouseDown = true;
	_lastPoint = QPoint(event->x(), event->y());
}

void DrawWidget::mouseReleaseEvent(QMouseEvent *event) {
	//qDebug() << "mouseReleaseEvent" << event->button() << event->x() << event->y();
	
	Q_UNUSED(event);

	_mouseDown = false;
}

void DrawWidget::paintEvent(QPaintEvent *event) {
	Q_UNUSED(event);

	QPainter painter(this);
	painter.drawPixmap(event->rect(), _pixmap, event->rect());
}

// kate: space-indent off; indent-width 8; replace-tabs off;
