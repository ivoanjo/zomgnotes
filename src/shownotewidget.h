/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHOWNOTEWIDGET_H
#define SHOWNOTEWIDGET_H

#include "drawwidget.h"
#include "note.h"

#include <QWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QScrollArea>
#include <QPushButton>

class ShowNoteWidget : public QWidget {
	Q_OBJECT

public:
	ShowNoteWidget(QWidget *parent = 0);
	~ShowNoteWidget();

	void setNote(Note *note);
	Note* getNote();

public slots:
	void closeNote();
	void confirmDeleteNote();

signals:
	void doneNote();
	void newNote();
	void deleteNote();

public:
	QPushButton *_toggleFullscreenButton;

private:
	DrawWidget *_drawWidget;
	QLabel *_noteNameLabel;
	QLineEdit *_noteNameEdit;
	QLabel *_dateLabel;
	QComboBox *_comboCategory;
	QScrollArea *_drawScrollArea;
	Note *_currentNote;
};

#endif // SHOWNOTEWIDGET_H

// kate: space-indent off; indent-width 8; replace-tabs off;
