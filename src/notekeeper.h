/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTEKEEPER_H
#define NOTEKEEPER_H

#include <QStringList>
#include <QList>

#include "note.h"

class NoteKeeper {

public:
	static void initialize();
	static Note* newNote();
	static QStringList categories();
	static QList<Note*> notesForCategory(QString category);
	static Note* getNote(QString noteId);
	static bool saveNote(Note *note);
	static bool deleteNote(Note *note);

private:
	static int randomNumber();
	static void loadNotes();

private:
	static QList<Note*> _noteList;
	static QString _dataDir;
};

#endif

// kate: space-indent off; indent-width 8; replace-tabs off;
