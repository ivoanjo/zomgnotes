/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "notelistwidget.h"

#include <QVBoxLayout>
#include <QComboBox>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDir>
#include <QtDebug>
#include <QHeaderView>

#include "notekeeper.h"

#include "global.h"

#define NOTE_ID_ROLE Qt::UserRole

NoteListWidget::NoteListWidget(QWidget* parent) : QWidget(parent), _notesTreeWidget(0), _comboCategory(0) {
	QVBoxLayout *vboxLayout = new QVBoxLayout();
	{
	int oldTop, oldBottom;
	vboxLayout->getContentsMargins(0, &oldTop, 0, &oldBottom);
	vboxLayout->setContentsMargins(0, oldTop, 0, oldBottom);
	}

	_comboCategory = new QComboBox();
	vboxLayout->addWidget(_comboCategory);

	_comboCategory->addItems(NoteKeeper::categories());

	_notesTreeWidget = new QTreeWidget();
	_notesTreeWidget->setFrameShape(QFrame::NoFrame);
	_notesTreeWidget->setColumnCount(2);
	_notesTreeWidget->setRootIsDecorated(false);
	_notesTreeWidget->setHeaderHidden(true);
	_notesTreeWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	_notesTreeWidget->header()->setResizeMode(QHeaderView::ResizeToContents);

	_notesTreeWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	vboxLayout->addWidget(_notesTreeWidget);

	QHBoxLayout *hboxLayout = new QHBoxLayout();

	QPushButton *newButton = new QPushButton(tr("New"));
	hboxLayout->addWidget(newButton);
	hboxLayout->addStretch();

	_toggleFullscreenButton = new QPushButton();
	_toggleFullscreenButton->setIcon(QIcon(":/icons/view-fullscreen.png"));
	_toggleFullscreenButton->setFlat(true);
	_toggleFullscreenButton->setCheckable(true);
	hboxLayout->addWidget(_toggleFullscreenButton);

	vboxLayout->addLayout(hboxLayout);

	setLayout(vboxLayout);

	connect(_notesTreeWidget, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
		this, SLOT(itemSelected(QTreeWidgetItem*)));
	connect(newButton, SIGNAL(clicked()), this, SIGNAL(newNote()));
	connect(_comboCategory, SIGNAL(currentIndexChanged(int)),
		this, SLOT(updateNoteList()));

	updateNoteList();
}

void NoteListWidget::updateNoteList() {
	//qDebug() << "updateNoteList";
	_notesTreeWidget->clear();

	QList<Note*> notes = NoteKeeper::notesForCategory(NoteKeeper::categories().at(_comboCategory->currentIndex()));

	int count = 0;
	foreach (Note *note, notes) {
		QStringList info;
		info << QString::number(++count) + ". " + note->title();
		info << note->simpleDate();

		QTreeWidgetItem *item = new QTreeWidgetItem(_notesTreeWidget, info);
		item->setData(0, NOTE_ID_ROLE, note->id());
		QFont f = item->font(0);
		f.setPointSize(f.pointSize() * 1.5);
		item->setFont(0, f);
		item->setFont(1, f);
	}
}

void NoteListWidget::itemSelected(QTreeWidgetItem *item) {
	//qDebug() << "itemSelected" << item->data(0, NOTE_ID_ROLE);
	emit loadNote(item->data(0, NOTE_ID_ROLE).toString());
}

// kate: space-indent off; indent-width 8; replace-tabs off;
