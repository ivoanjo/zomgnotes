/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"

#include "global.h"
#include "notekeeper.h"

#include <QApplication>
#include <QAction>
#include <QMenuBar>
#include <QMessageBox>

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);

	QCoreApplication::setApplicationVersion(ZOMGNOTES_VERSION);
	QCoreApplication::setApplicationName("zomgnotes");
	QCoreApplication::setOrganizationName("zomgnotes");

#ifndef Q_OS_SYMBIAN
	QApplication::setStyle("cleanlooks");
#endif

	QWidget *mainWindow = new MainWindow();

#ifndef Q_OS_SYMBIAN
	mainWindow->show();
#else
	mainWindow->showMaximized();
#endif

	return app.exec();
}

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), _stackedLayout(0), _noteListWidget(0), _showNoteWidget(0), _fullscreen(false) {

	NoteKeeper::initialize();

	QWidget *widget = new QWidget();
	_stackedLayout = new QStackedLayout();

	_noteListWidget = new NoteListWidget(this);
	_stackedLayout->addWidget(_noteListWidget);

	_showNoteWidget = new ShowNoteWidget(this);
	_stackedLayout->addWidget(_showNoteWidget);

	widget->setLayout(_stackedLayout);
	setCentralWidget(widget);

	connect(_noteListWidget, SIGNAL(loadNote(QString)), this, SLOT(loadNote(QString)));
	connect(_noteListWidget, SIGNAL(newNote()), this, SLOT(newNote()));
	connect(_noteListWidget->_toggleFullscreenButton, SIGNAL(toggled(bool)),
		this, SLOT(toggleFullscreen(bool)));

	connect(_showNoteWidget, SIGNAL(doneNote()), this, SLOT(doneNote()));
	connect(_showNoteWidget, SIGNAL(newNote()), this, SLOT(doneNewNote()));
	connect(_showNoteWidget, SIGNAL(deleteNote()), this, SLOT(deleteNote()));
	connect(_showNoteWidget->_toggleFullscreenButton, SIGNAL(toggled(bool)),
		this, SLOT(toggleFullscreen(bool)));

	// Menubar
	QAction *showSettingsAct = new QAction(tr("Settings"), this);
	connect(showSettingsAct, SIGNAL(triggered()), this, SLOT(showSettings()));
	QAction *aboutAct = new QAction(tr("About"), this);
	connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

	QMenuBar *mBar = menuBar();
	#ifdef EXPERIMENTAL
	mBar->addAction(showSettingsAct);
	#endif
	mBar->addAction(aboutAct);
}

MainWindow::~MainWindow() { }

void MainWindow::closeEvent(QCloseEvent *event) {
	if (_stackedLayout->currentIndex() == 1) doneNote();
	event->accept();
}

void MainWindow::loadNote(QString noteId) {
	qDebug() << "loadNote" << noteId;
	_showNoteWidget->setNote(NoteKeeper::getNote(noteId));
	_stackedLayout->setCurrentIndex(1);
}

void MainWindow::newNote() {
	qDebug() << "newNote";
	Note *note = NoteKeeper::newNote();
	_showNoteWidget->setNote(note);
	_stackedLayout->setCurrentIndex(1);
}

void MainWindow::doneNote() {
	qDebug() << "doneNote";
	_showNoteWidget->closeNote();
	NoteKeeper::saveNote(_showNoteWidget->getNote());
	updateNoteList();
	_stackedLayout->setCurrentIndex(0);
}

void MainWindow::doneNewNote() {
	qDebug() << "doneNewNote";
	_showNoteWidget->closeNote();
	NoteKeeper::saveNote(_showNoteWidget->getNote());
	newNote();
}

void MainWindow::deleteNote() {
	qDebug() << "deleteNote";
	_showNoteWidget->closeNote();
	NoteKeeper::deleteNote(_showNoteWidget->getNote());
	updateNoteList();
	_stackedLayout->setCurrentIndex(0);
}

void MainWindow::updateNoteList() {
	_noteListWidget->updateNoteList();
}

void MainWindow::toggleFullscreen(bool checked) {
	if (checked != _fullscreen) {
		_fullscreen = checked;
		if (checked) {
			showFullScreen();
		} else {
			#ifdef Q_OS_SYMBIAN
			showMaximized();
			#else
			showNormal();
			#endif
		}
		_noteListWidget->_toggleFullscreenButton->setChecked(checked);
		_showNoteWidget->_toggleFullscreenButton->setChecked(checked);
	}
}

void MainWindow::showSettings() {
	qDebug() << "showSettings!";
}

void MainWindow::about() {
	QMessageBox::about(this, tr("About zomgnotes"),
		"<h3>zomgnotes " + QCoreApplication::applicationVersion() + "</h3> \
		<br>" + tr("A simple note drawing application") + ".<p>" + tr("Visit us at") + " \
		<a href='https://github.com/ivoanjo/zomgnotes/'>https://github.com/ivoanjo/zomgnotes</a> \
		<p> \
		<small>Copyright (C) 2009 Ivo Anjo<br> \
		" + tr("Licensed under the GNU GPLv3 or later") + "\
		&lt;<a href='http://gnu.org/licenses/gpl.html'>http://gnu.org/licenses/gpl.html</a>&gt;<br> \
		" + tr("This is free software: you are free to change and redistribute it.<br> \
		There is NO WARRANTY, to the extent permitted by law.") + "</small>");
}

// kate: space-indent off; indent-width 8; replace-tabs off;
