/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GLOBAL_H
#define GLOBAL_H

// Disable EXPERIMENTAL features by default on symbian
// ...and also on Maemo/Hildon
#ifndef Q_OS_SYMBIAN
#ifndef Q_WS_HILDON

#define EXPERIMENTAL

#endif
#endif

#ifdef EXPERIMENTAL
#warning EXPERIMENTAL features ON
#endif

// Work around CRAPPY compilers
#ifndef __GNUC__
#undef ZOMGNOTES_VERSION
#define ZOMGNOTES_VERSION "crappy-compiler"
#endif

#endif
