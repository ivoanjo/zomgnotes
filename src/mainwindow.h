/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedLayout>
#include <QCloseEvent>

#include "notelistwidget.h"
#include "shownotewidget.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void loadNote(QString noteId);
	void newNote();

	void doneNote();
	void doneNewNote();
	void deleteNote();
	void toggleFullscreen(bool checked);

	void updateNoteList();

	// Triggered by menubar actions
	void showSettings();
	void about();

protected:
	virtual void closeEvent(QCloseEvent *event);

private:
	QStackedLayout *_stackedLayout;
	NoteListWidget *_noteListWidget;
	ShowNoteWidget *_showNoteWidget;
	bool _fullscreen;
};

#endif // MAINWINDOW_H

// kate: space-indent off; indent-width 8; replace-tabs off;
