/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTE_H
#define NOTE_H

#include <QString>
#include <QImage>
#include <QDateTime>
#include <QMap>

#define NOTE_IMAGE_FORMAT QImage::Format_Mono

class Note {

friend class NoteKeeper;

private:
	Note();
	~Note();

public:
	QString id();
	QString title();
	QString category();
	QDateTime date();
	QImage image();

	QString simpleDate();

	void setTitle(QString title);
	void setCategory(QString category);
	void setImage(QImage image);

	//bool operator ==(Note note) const;
	//bool operator !=(Note note) const;
	//bool operator <(const Note note) const;

	static bool dateLessThan(const Note *n1, const Note *n2);

private:
	QString _id;
	QString _title;
	QString _category;
	QDateTime _date;
	QImage _image;
	QMap<QString,QString> _additionalMetadata;
};

#endif

// kate: space-indent off; indent-width 8; replace-tabs off;
