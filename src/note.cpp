/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "note.h"

#include <QtDebug>

Note::Note() { }

Note::~Note() { }

QString Note::id() { return _id; }
QString Note::title() { return _title; }
QString Note::category() { return _category; }
QDateTime Note::date() { return _date; }
QImage Note::image() { return _image; }

QString Note::simpleDate() { return _date.toString("d/M"); }

void Note::setTitle(QString title) { _title = title; }
void Note::setCategory(QString category) { _category = category; }
void Note::setImage(QImage image) { _image = image; }

// bool Note::operator ==(Note n) const { return _id == n._id; }
// bool Note::operator !=(Note n) const { return !operator ==(n); }
// bool Note::operator <(const Note n) const { return _date < n._date; }

bool Note::dateLessThan(const Note *n1, const Note *n2) {
	return n1->_date < n2->_date;
}

// kate: space-indent off; indent-width 8; replace-tabs off;
