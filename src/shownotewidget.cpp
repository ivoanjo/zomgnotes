/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "shownotewidget.h"

#include "notekeeper.h"
#include "global.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QFile>
#include <QDate>
#include <QMap>
#include <QTextEdit>
#include <QScrollBar>

ShowNoteWidget::ShowNoteWidget(QWidget* parent) :
	QWidget(parent), _drawWidget(0), _noteNameLabel(0), _noteNameEdit(0), _dateLabel(0), _comboCategory(0), _drawScrollArea(0),  _currentNote(0) {
	QVBoxLayout *vboxLayout = new QVBoxLayout();
	{
	int oldTop, oldBottom;
	vboxLayout->getContentsMargins(0, &oldTop, 0, &oldBottom);
	vboxLayout->setContentsMargins(0, oldTop, 0, oldBottom);
	}

	// Form header
	QHBoxLayout *headerLayout = new QHBoxLayout();

	_noteNameLabel = new QLabel();
	headerLayout->addWidget(_noteNameLabel);

	headerLayout->addStretch();

#ifdef EXPERIMENTAL
	QPushButton *previousNoteButton = new QPushButton();
	previousNoteButton->setIcon(QIcon(":/icons/arrow-left.png"));
	previousNoteButton->setFlat(true);
	headerLayout->addWidget(previousNoteButton);

	QLabel *countLabel = new QLabel("x of y");
	headerLayout->addWidget(countLabel);

	QPushButton *nextNoteButton = new QPushButton();
	nextNoteButton->setIcon(QIcon(":/icons/arrow-right.png"));
	nextNoteButton->setFlat(true);
	headerLayout->addWidget(nextNoteButton);

	headerLayout->addStretch();
#endif

	_comboCategory = new QComboBox();
	headerLayout->addWidget(_comboCategory);

	QStringList categories = NoteKeeper::categories();
	categories.removeOne("All");
	_comboCategory->addItems(categories);

	vboxLayout->addLayout(headerLayout);

	// Scroll area central
	_drawScrollArea = new QScrollArea();
	_drawScrollArea->setFrameShape(QFrame::NoFrame);
	vboxLayout->addWidget(_drawScrollArea);

	QVBoxLayout *vboxScrollAreaLayout = new QVBoxLayout();
	{
	int oldTop, oldBottom;
	vboxScrollAreaLayout->getContentsMargins(0, &oldTop, 0, &oldBottom);
	vboxScrollAreaLayout->setContentsMargins(0, oldTop, 0, oldBottom);
	}

	// Scroll area header
	QHBoxLayout *headerScrollAreaLayout = new QHBoxLayout();

	_noteNameEdit = new QLineEdit();
	connect(_noteNameEdit, SIGNAL(textChanged(const QString &)),
		_noteNameLabel, SLOT(setText(const QString &)));
	headerScrollAreaLayout->addWidget(_noteNameEdit);

	_dateLabel = new QLabel();
	headerScrollAreaLayout->addWidget(_dateLabel);

#ifdef EXPERIMENTAL
	QPushButton *alarmButton = new QPushButton();
	alarmButton->setIcon(QIcon(":/icons/chronometer.png"));
	alarmButton->setFlat(true);
	headerScrollAreaLayout->addWidget(alarmButton);
#endif

	vboxScrollAreaLayout->addLayout(headerScrollAreaLayout);

	_drawWidget = new DrawWidget();
	vboxScrollAreaLayout->addWidget(_drawWidget);

	QWidget *scrollAreaWidget = new QWidget();
	scrollAreaWidget->setLayout(vboxScrollAreaLayout);
	_drawScrollArea->setWidget(scrollAreaWidget);
	_drawScrollArea->setWidgetResizable(true);

	// Form footer
	QHBoxLayout *footerLayout = new QHBoxLayout();

	QPushButton *doneButton = new QPushButton(tr("Done"));
	footerLayout->addWidget(doneButton);

	QPushButton *newButton = new QPushButton(tr("New"));
	footerLayout->addWidget(newButton);

	QPushButton *deleteButton = new QPushButton(tr("Delete"));
	footerLayout->addWidget(deleteButton);

#ifdef EXPERIMENTAL
	QComboBox *penStyleCombo = new QComboBox();
	penStyleCombo->addItem(""); // Fine
	penStyleCombo->setItemIcon(0, QIcon(":/icons/draw-freehand.png"));
	penStyleCombo->addItem(""); // Normal
	penStyleCombo->setItemIcon(1, QIcon(":/icons/draw-freehand.png"));
	penStyleCombo->addItem(""); // Thick
	penStyleCombo->setItemIcon(2, QIcon(":/icons/draw-freehand.png"));
	penStyleCombo->addItem(""); // Eraser
	penStyleCombo->setItemIcon(3, QIcon(":/icons/draw-eraser.png"));
	penStyleCombo->setCurrentIndex(1); // Normal
	footerLayout->addWidget(penStyleCombo);
#endif

	footerLayout->addStretch();

	_toggleFullscreenButton = new QPushButton();
	_toggleFullscreenButton->setIcon(QIcon(":/icons/view-fullscreen.png"));
	_toggleFullscreenButton->setFlat(true);
	_toggleFullscreenButton->setCheckable(true);
	footerLayout->addWidget(_toggleFullscreenButton);

	vboxLayout->addLayout(footerLayout);

	setLayout(vboxLayout);

	// Connect signal-to-signal
	connect(doneButton, SIGNAL(clicked()), this, SIGNAL(doneNote()));
	connect(newButton, SIGNAL(clicked()), this, SIGNAL(newNote()));
	connect(deleteButton, SIGNAL(clicked()), this, SLOT(confirmDeleteNote()));
}

ShowNoteWidget::~ShowNoteWidget() { }

void ShowNoteWidget::setNote(Note *note) {
	_currentNote = note;
	// Set the title
	_noteNameEdit->setText(_currentNote->title());
	// Set the date
	_dateLabel->setText(_currentNote->simpleDate());
	// Set the category
	int idx = _comboCategory->findText(_currentNote->category());
	if (idx == -1) idx = 0;
	_comboCategory->setCurrentIndex(idx);
	// Set the image
	_drawWidget->setImage(_currentNote->image());
	// Small optimization: set the note with an empty image, since the correct, updated
	// one will be set when closeNote() is called
	_currentNote->setImage(QImage());
	// Reset scroll on the QScrollArea
	_drawScrollArea->verticalScrollBar()->setValue(0);
}

Note* ShowNoteWidget::getNote() {
	return _currentNote;
}

void ShowNoteWidget::closeNote() {
	_currentNote->setTitle(_noteNameEdit->text());
	_currentNote->setCategory(_comboCategory->currentText());
	_currentNote->setImage(_drawWidget->getImage());
}

void ShowNoteWidget::confirmDeleteNote() {
	if (QMessageBox::question(this, tr("Delete Note"),
		"Do you really want to delete this note?",
		QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
		== QMessageBox::Yes) {
		emit deleteNote();
	}
}

// kate: space-indent off; indent-width 8; replace-tabs off;
