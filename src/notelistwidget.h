/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTELISTWIDGET_H
#define NOTELISTWIDGET_H

#include <QWidget>
#include <QTreeWidget>
#include <QComboBox>
#include <QPushButton>

class NoteListWidget : public QWidget {
	Q_OBJECT

public:
	NoteListWidget(QWidget *parent = 0);

public slots:
	void updateNoteList();

private slots:
	void itemSelected(QTreeWidgetItem *item);

signals:
	void loadNote(QString fileName);
	void newNote();

public:
	QPushButton *_toggleFullscreenButton;

private:
	QTreeWidget *_notesTreeWidget;
	QComboBox *_comboCategory;
};

#endif // NOTELISTWIDGET_H

// kate: space-indent off; indent-width 8; replace-tabs off;
