/*
 *  Copyright (C) 2009 Ivo Anjo <knuckles@gmail.com>
 *
 *  This file is part of zomgnotes.
 *
 *  zomgnotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  zomgnotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with zomgnotes.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DRAWWIDGET_H
#define DRAWWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QtDebug>
#include <QMap>

class DrawWidget : public QLabel
{
	Q_OBJECT

public:
	DrawWidget(QWidget *parent = 0);
	void setImage(QImage newImage);
	QImage getImage();

protected:
	void mouseMoveEvent(QMouseEvent *event);
	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);

private:
	void draw(const QPoint & p1, const QPoint & p2);

private:
	QPixmap _pixmap;
	bool _mouseDown;
	QPoint _lastPoint;
};

#endif // DRAWWIDGET_H

// kate: space-indent off; indent-width 8; replace-tabs off;
