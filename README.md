# zomgnotes

zomgnotes is a simple Qt-based note-taking app for symbian.

I've uploaded it here for historical reasons, feel free to do whatevers with it!

## Building

To build/run on unix platforms:

	qmake zomgnotes.pro
	make
	./zomgnotes

I suggest doing out-of-tree builds:

	mkdir build
	cd build
	qmake ../zomgnotes.pro
	make
	./zomgnotes

To build for symbian (on windows, unfortunately):

	qmake zomgnotes.pro

	# to run in emulator
	make
	make run

	# for phone, debug build
	make debug-gcce
	createpackage -i zomgnotes_gcce_udeb.pkg

	# for phone, release build
	make release-gcce
	createpackage -i zomgnotes_gcce_urel.pkg
